# Laravel One To Many
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-one-to-many.git`
2. Go inside the folder: `cd laravel-one-to-many`
3. Set your desired database name in `.env` file
4. Run `laravel_OneToMany.sql` file in your database.
5. Run `php artisan serve`
6. Open your favorite browser: http://localhost:8000/article

### Screen shot

Table Relationship

![Table Relationship](img/relasi.png "Table Relationship")

Web Article

![Web Article](img/article.png "Web Article") 
